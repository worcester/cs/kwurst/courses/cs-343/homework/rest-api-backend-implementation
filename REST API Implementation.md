# REST API Backend Implementation Homework

## Instructions

**You will be making your changes to the files in your GuestInfoBackend fork for this assignment.** `Worcester State University / Computer Science Department / CS 343 01 02 Fall 2024 / Students/ username / microservices-theaspantry / guestinfosystem / guestinfobackend`

**You do not need to Fork the REST API Backend Implementation Homework to your private GitLab subgroup. You will not be submitting your homework through that repository.**

## Guest Info API `v2.1.1-Fall2023.REST-API-Backend-Implementation-HW`

Included in *this* repository (REST API Backend Implementation Homework) is a new a copy of the GuestInfoAPI `v2.1.1-Fall2023.REST-API-Backend-Implementation-HW` ([openapi.yaml](openapi.yaml)).

You will need to copy it into your GuestInfoBackend clone, replacing the copy in `lib`.

This includes all of the new endpoints you will be asked to implement in this homework. You will want to view this new API specification using the Swagger Preview to see what you are implementing.

## *Base Assignment*

**Everyone must complete this portion of the assignment in a *Meets Specification* fashion.**

### Implement the new REST API endpoint for `PUT /guests/{wsuID}/assistance`

1. Add a new file in `src/endpoints` to implement the new endpoint.
2. Add or modify any needed support functions in `src/Data/guest.js`.
3. Build the backend.
4. Test your new endpoint by creating a series of calls in a file called `testing/manual/base1.http`. Include all calls needed to test your endpoint ***in the order they need to be called***.

### Implement the new REST API endpoints for `GET /guests`

1. Modify the `src/listGuests.js` to implement:

    1. `GET /guests?ageEQ=` - guestAge equal to
    2. `GET /guests?ageLT=` - guestAge less than
    3. `GET /guests?ageGT=` - guestAge greater than
    4. `GET /guests?ageLTEQ=` - guestAge less than or equal to
    5. `GET /guests?ageGTEQ=` - guestAge greater than or equal to

    * All should be optional parameters.
    * Currently, there is no way in OpenAPI to specify that only one can be used at a time. Instead, add logic to prevent the use of more than one parameter, and return a 400 response if more than one of the 5 query paramters above is given.

2. Add or modify any needed support functions in `src/Data/guest.js`.'
3. Build the backend.
4. Test your new endpoint by creating a series of calls in a file called `testing/manual/base2.http`. Include all calls needed to test your endpoint ***in the order they need to be called***.

#### Once you are happy with your work, commit your changes with the message `Base Assignment`

## *Intermediate Add-On*

**Complete this portion in a *Meets Specification* fashion to apply towards base course grades of C or higher**

* See the the Course Grade Determination table in the syllabus to see how many Intermediate Add-Ons you need to submit for a particular letter grade.
* See My Grades in Blackboard to see how many you have submitted and received an Acceptable grade on.
* You may not need to submit one for this assignment.

### Implement the new REST API endpoint for `GET /inventory`

1. Add a new file in `src/endpoints` to implement the new endpoint.
2. Add a new file `src/Data/inventory.js` with appropriate support functions to communicate with a different database and collection for inventory. **Note: You do not need to reimplement all of the functions in `src/Data/guest.js` - only the ones needed for the `GET` endpoint.**
3. Build the backend.
4. Test your new endpoint by creating a series of calls in a file called `testing/manual/intermediate.http`. Include all calls needed to test your endpoint ***in the order they need to be called***.

#### Once you are happy with your work, commit your changes with the message `Intermediate Assignment`.

## *Advanced Add-On*

**Complete this portion in a *Meets Specification* fashion to apply towards base course grades of B or higher**

* See the the Course Grade Determination table in the syllabus to see how many Intermediate Add-Ons you need to submit for a particular letter grade.
* See My Grades in Blackboard to see how many you have submitted and received an Acceptable grade on.
* You may not need to submit one for this assignment.

### Implement the new REST API endpoint for `PATCH /inventory?change=`*pounds*`%wsuID=`*id*

* Both query parameters are required
* 200 Response, should return the number of pounds in the inventory
* 400 Response, if missing either change or wsuID

1. Add a new file in `src/endpoints` to implement the new endpoint.
    * The endpoint must update the inventory database **and** send a message to the Message Broker with the change value and wsuID.
2. Modify `src/Data/inventory.js` with appropriate support functions to communicate with a different database and collection for inventory. **Note: You do not need to reimplement all of the functions in `src/Data/guest.js` - only the ones needed for this `PATCH` endpoint.**
3. Build the backend.
4. Test your new endpoint by creating a series of calls in a file called `testing/manual/advanced1.http`. Include all calls needed to test your endpoint ***in the order they need to be called***.

### Implement the new REST API endpoint for `PATCH /guests/{wsuID}/assistance

A new endpoint to modify the Guest's Assistance object. The advantage to this is that we will not need to pass the full Guest assistance object in the request body (repeating information that is not changing.) We pass an Assistance object **with only the fields that are changing** in the request body. You should return the full guest in the response body.

One of the challenges for this is that we need a new schema. The `Assistance` schema we already have lists all fields as `required`. In the api you will find a new schema called `AssistanceOptional` with the `required` section removed.

The other challenge is figuring out which of the fields are in the `AssistanceOptional` object passed in the request body. The brute force way to do this would be to use 9 `if` statements to check for the existence of each field. This seems like lots of repetition and difficult to change if new fields are added to the schema.

Fortunately, JavaScript provides a `for...in` statement [(docs)](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Statements/for...in) that allows you to iterate over all of the properties (fields) in an object. So, we can do something like:

```javascript
// ...
const id = request.params.id;
const assistanceData = request.body;
var guest = await Guests.getOne(id);
if (guest !== null) {

  for (const property in assistanceData) {
    guest.assistance[`${property}`] = assistanceData[`${property}`];
  }
// ... more code including updating the guest, sending the changes to the message broker, and returning the response
```

``` `${property}` ``` is a template string that lets you convert the value of the variable `property` to a string. That string value is then used as a key to access a field in the object.

1. Add a new file in `src/endpoints` to implement the new endpoint.
    * Base it on `replaceGuest.js`.
2. Add or modify any needed support functions in `src/Data/guest.js`.
3. Build the backend.
4. Test your new endpoint by creating a series of calls in a file called `testing/manual/advanced2.http`. Include all calls needed to test your endpoint ***in the order they need to be called***.

#### Once you are happy with your work, commit your changes with the message `Advanced Assignment`

---

&copy; 2024 Karl R. Wurst <karl@w-sts.com>

<!-- markdownlint-disable MD033 -->
<img src="https://mirrors.creativecommons.org/presskit/buttons/88x31/png/by-sa.png" width=100px/>This work is licensed under the Creative Commons Attribution-ShareAlike 4.0 International License. To view a copy of this license, visit [http://creativecommons.org/licenses/by-sa/4.0/](http://creativecommons.org/licenses/by-sa/4.0/) or send a letter to Creative Commons, 444 Castro Street, Suite 900, Mountain View, California, 94041, USA.
